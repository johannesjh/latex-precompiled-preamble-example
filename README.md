# Example of Using a Pre-Compiled LateX Preamble

The [latex-precompiled-preamble-example repository](https://bitbucket.org/johannesjh/latex-precompiled-preamble-example/) provides an example of how to pre-compile a LateX preamble into a custom format file to speed up subsequent document compilations.

## How to try this example

### 1. Pre-compile the preamble

Run the following command:
`pdftex -ini -jobname="preamble" "&pdflatex" mylatexformat.ltx precompile`

...this should provide you with a preamble.fmt file.


### 2. Use the pre-compiled preamble to write your thesis

Run the following command:
`pdflatex thesis`



## License

To the extent possible under law, Johannes Harms has waived all copyright and related or neighboring rights to [Example of Using a Pre-Compiled LateX Preamble](https://bitbucket.org/johannesjh/latex-precompiled-preamble-example). This work is published from: Austria. 



## Credits

Thanks to [Frits Wenneker's blogpost](http://www.howtotex.com/tips-tricks/faster-latex-part-iv-use-a-precompiled-preamble/) where he explains details on how to use the [mylatexformat](https://www.ctan.org/pkg/mylatexformat) package.
