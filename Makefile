BASENAME=thesis

# OS detection
OS=$(shell uname)

# pdf viewer
ifeq ($(OS), Darwin)
	VIEWER=open
	# VIEWER_OPTIONS= -a Skim
else
	VIEWER=acroread
	VIEWER_OPTIONS=
endif


.PHONY: default
default: clean compile view


compile:
	pdflatex $(BASENAME)
	# bibtex $(BASENAME)
	# biber $(BASENAME)
	# makeglossaries $(BASENAME)
	pdflatex $(BASENAME)
	pdflatex $(BASENAME)


view:
	$(VIEWER) $(VIEWER_OPTIONS) $(BASENAME).pdf


.PHONY: clean
clean:
	rm -f *.acn *.aux *.bbl *.bcf *.bla *.blg *.dvi *.haux *.htoc *image.tex *.loa *.lof *.log *.lot *.out *.tdo *.toc *-blx.bib *run.xml *.cut *.glo *.gxg *.glx *.ilg *.lol *.gxs *.acn *.glo *.ist *.acr *.alg *.glg *.gls *.idx *.ind *.maf *.mtc *.mtc0 *.nav *.snm *.xdy *.synctex.gz *'synctex.gz(busy)'
	find . -name '*.aux' -print0 | xargs -0 rm
	rm -f $(BASENAME).pdf $(BASENAME).html

